#ifndef WEBPAGE_H_
#define WEBPAGE_H_

#include <QtWebKit>
#include <QDebug>

class WebPage : public QWebPage
{
	Q_OBJECT

	public:
		 WebPage(QObject *parent = 0);
		~WebPage();

		QString	userAgent;

	private:
		QString userAgentForUrl(const QUrl &url ) const;
	    void	javaScriptConsoleMessage(const QString& message, int lineNumber, const QString& sourceID);
	    void 	javaScriptAlert(QWebFrame *originatingFrame, const QString& msg);
	    bool 	javaScriptConfirm(QWebFrame *originatingFrame, const QString& msg);
	    bool 	javaScriptPrompt(QWebFrame *originatingFrame, const QString& msg, const QString& defaultValue, QString* result);
};

#endif /*WEBPAGE_H_*/

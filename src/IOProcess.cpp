#include "IOProcess.h"

IOProcess::IOProcess(QObject *parent)
{
	this->_isRunning = false;

	connect(&this->_timer, SIGNAL(timeout()), this, SLOT(slotUpdateTimeElapsed()));
	connect(&this->_process, SIGNAL(started()), this, SLOT(slotStarted()));
	connect(&this->_process, SIGNAL(stateChanged(QProcess::ProcessState)), this, SLOT(slotStateChanged(QProcess::ProcessState)));
	connect(&this->_process, SIGNAL(readyReadStandardOutput()), this, SLOT(slotReadyReadStandardOutput()));
	connect(&this->_process, SIGNAL(readyReadStandardError()), this, SLOT(slotReadyReadStandardError()));
	connect(&this->_process, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(slotProcessFinished(int, QProcess::ExitStatus)));
    connect(&this->_process, SIGNAL(error(QProcess::ProcessError)), this, SLOT(slotProcessError(QProcess::ProcessError)));
}

IOProcess::~IOProcess()
{
	this->_process.close();
}

void IOProcess::run(QString commmand)
{
	if(this->_isRunning)
	{
		this->kill();
	}

	this->_process.start(commmand);
	this->_process.waitForStarted(5 * 1000);
}

void IOProcess::wait(int timeout)
{
	this->_timeout 		= timeout;
	this->_timeElapsed	= 0;
	this->_timer.start(1000);

	while(this->_isRunning && (this->_timeElapsed < this->_timeout))
    {
    	qApp->processEvents(QEventLoop::WaitForMoreEvents);
    }

	this->_timer.stop();
}

bool IOProcess::isRunning()
{
	return this->_isRunning;
}

void IOProcess::kill()
{
	this->_process.close();
}

void IOProcess::stdIn(QByteArray data)
{
	this->_process.write(data);
}

void IOProcess::stdInClose()
{
	this->_process.closeWriteChannel();
}

QByteArray IOProcess::stdOut()
{
	return this->_stdOut;
}

QByteArray IOProcess::stdError()
{
	return this->_stdError;
}

int IOProcess::statusCode()
{
	return this->_code;
}

void IOProcess::slotUpdateTimeElapsed()
{
	this->_timeElapsed++;
}

void IOProcess::slotStarted(){}

void IOProcess::slotStateChanged(QProcess::ProcessState newState)
{
	this->_isRunning = newState == QProcess::Running;
}

void IOProcess::slotReadyReadStandardOutput()
{
	this->_stdOut += this->_process.readAllStandardOutput();	//QString::fromLocal8Bit(newData);
}

void IOProcess::slotReadyReadStandardError()
{
	this->_stdError += this->_process.readAllStandardError();	//QString::fromLocal8Bit(newData);
}

void IOProcess::slotProcessFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
	if (exitStatus == QProcess::CrashExit)
	{
		this->_code = 2;

	}else if (exitCode != 0)
	{
		this->_code = 1;

	}else
	{
		this->_code = 0;
    }

	this->_process.close();
}

void IOProcess::slotProcessError(QProcess::ProcessError error)
{
	if(error == QProcess::FailedToStart)
	{
        this->_code = 3;
    }
}

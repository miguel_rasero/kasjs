#include "FileSystem.h"

FileSystem::FileSystem(QObject *parent){}

FileSystem::~FileSystem()
{
	QMapIterator<QString, QFile *> i(this->_files);
	while ( i.hasNext() )
	{
		i.next();

		this->_files[i.key()]->close();
	}

	this->_files.clear();
}

QFlag FileSystem::mode(QString mode)
{
	if(mode == "r")
		return QIODevice::ReadOnly;

	if(mode == "w")
			return  QIODevice::WriteOnly;

	if(mode == "w+")
			return QIODevice::Truncate;

	if(mode == "a")
			return QIODevice::Append;

	return QIODevice::ReadWrite;
}

QByteArray FileSystem::encode(QString data, QString encoding)
{
	QTextCodec *codec		= QTextCodec::codecForName(encoding.toAscii());
	QByteArray dataEncoded	= codec->fromUnicode(data);

	return dataEncoded;
}
/***************************************SLOTS****************************************/

bool FileSystem::dirExists(QString dirName)
{
	QDir dir;

	return dir.exists(dirName);
}

bool FileSystem::dirCreate(QString dirName)
{
	QDir dir;

	return dir.mkpath(dirName);
}

bool FileSystem::dirDelete(QString dirName)
{
	QDir dir;

	return dir.rmdir(dirName);
}

bool FileSystem::dirDeletePath(QString dirName)
{
	QDir dir;

	return dir.rmpath(dirName);
}

bool FileSystem::dirRename(QString dirName, QString dirNameNew)
{
	QDir dir;

	return dir.rename(dirName, dirNameNew);
}

QStringList FileSystem::dirList(QString dirName)
{
	QDir dir(dirName);

	return dir.entryList();
}

QString FileSystem::separator()
{
	return QString(QDir::separator());
}

bool FileSystem::fileExists(QString fileName)
{
	return QFile::exists(fileName);
}

QString FileSystem::fileOpen(QString fileName, QString mode)
{
	QString fileRef = "@" + fileName;

	this->fileClose(fileRef);
	this->_files[fileRef] = new QFile(fileName);

	if(this->_files[fileRef]->open( this->mode(mode) ))
	{
		return fileRef;
	}

	return "";
}

void FileSystem::fileClose(QString fileRef)
{
	if( this->_files.contains(fileRef) )
	{
		this->_files[fileRef]->close();
	}
}

QByteArray FileSystem::fileRead(QString fileRef)
{
	QByteArray data;

	if( this->_files.contains(fileRef) )
	{
		this->_files[fileRef]->seek(0);

		data = this->_files[fileRef]->readAll();
	}

	return data;
}

QByteArray FileSystem::fileReadLine(QString fileRef)
{
	QByteArray data;

	if( this->_files.contains(fileRef) )
	{
		data = this->_files[fileRef]->readLine();
	}

	return data;
}

void FileSystem::fileWriteBinary(QString fileRef, QByteArray data)
{
	if( this->_files.contains(fileRef) )
	{
		this->_files[fileRef]->write( data );
	}
}

void FileSystem::fileWrite(QString fileRef, QString data, QString encoding)
{
	this->fileWriteBinary(fileRef, this->encode(data, encoding));
}

void FileSystem::fileCopy(QString source, QString target)
{
	QFile file(source);
	file.copy(target);
}

void FileSystem::fileRename(QString fileName, QString newName)
{
	QFile file(fileName);
	file.rename(newName);
}

void FileSystem::fileRemove(QString fileName)
{
	QFile file(fileName);
	file.remove();
}

QVariant FileSystem::fileInfo(QString fileName)
{
	QFileInfo finfo(fileName);

	QMap<QString, QVariant> info;
	info.insert("created",		QString::number(finfo.created().toTime_t()));
	info.insert("lastModified", QString::number(finfo.lastModified().toTime_t()));
	info.insert("lastRead",		QString::number(finfo.lastRead().toTime_t()));
	info.insert("size",			QString::number(finfo.size()));

	return QVariant(info);
}

QString FileSystem::toString(QByteArray byteArray, QString encoding)
{
	if(encoding.toLower() == "ascii")
	{
		return QString::fromAscii(byteArray);
	}

	if(encoding.toLower() == "8859-1" || encoding.toLower() == "latin-1")
	{
		return QString::fromLatin1(byteArray);
	}

	return QString::fromUtf8(byteArray);
}

function KCGI()
{ 
	var parseQueryString = function(qs)
	{
		var result		= {};
		var parts		= qs.split("&");
		var partsLen	= parts.length;
		
		for (var i=0; i<partsLen; i++)
		{
			var item	= parts[i];
			var eq		= item.indexOf("=");
			var name	= "";
			var value	= "";
			
			if (eq == -1) 
			{
				name = item;
			
			}else
			{
				name	= item.substring(0, eq);
				value	= item.substring(eq + 1);
			}
			
			if (name.substring(name.length - 2) == "[]") 
			{ 
				name = name.substring(0, name.length - 2); 
			}
			
			mixIn(result, decode(name), decode(value));
		}
		
		return result;
	}
	
	var mixIn = function(result, name, value)
	{
		if (!(name in result))
		{
			result[name] = value;
			
		}else if (result[name] instanceof Array)
		{
			result[name].push(value);
			
		}else
		{
			result[name] = [result[name], value];
		}
	}

	var decode = function(str)
	{
		var s = str.replace(/\+/g, " ");
		
		try
		{ 
			s = decodeURIComponent(s); 
		
		}catch(e)
		{
			return s;
		}
		
		return s;
	}
	
	parseENVIROMENT = function()
	{
		self.ENVIROMENT							= {};
		self.ENVIROMENT["AUTH_TYPE"]			= core.getEnviromentValue("AUTH_TYPE");

		self.ENVIROMENT["CONTENT_TYPE"]			= core.getEnviromentValue("CONTENT_TYPE");
		self.ENVIROMENT["PATH_INFO"]			= core.getEnviromentValue("PATH_INFO");
		self.ENVIROMENT["PATH_TRANSLATED"]		= core.getEnviromentValue("PATH_TRANSLATED");
		self.ENVIROMENT["REMOTE_HOST"]			= core.getEnviromentValue("REMOTE_HOST");
		self.ENVIROMENT["REMOTE_IDENT"]			= core.getEnviromentValue("REMOTE_IDENT");
		self.ENVIROMENT["REMOTE_USER"]			= core.getEnviromentValue("REMOTE_USER");

		self.ENVIROMENT["GATEWAY_INTERFACE"]	= core.getEnviromentValue("GATEWAY_INTERFACE");
		self.ENVIROMENT["SCRIPT_NAME"]			= core.getEnviromentValue("SCRIPT_NAME");
		self.ENVIROMENT["HTTP_ACCEPT"]			= core.getEnviromentValue("HTTP_ACCEPT");
		self.ENVIROMENT["HTTP_USER_AGENT"]		= core.getEnviromentValue("HTTP_USER_AGENT");
		self.ENVIROMENT["REQUEST_METHOD"]		= core.getEnviromentValue("REQUEST_METHOD");
		self.ENVIROMENT["QUERY_STRING"]			= core.getEnviromentValue("QUERY_STRING");
		self.ENVIROMENT["CONTENT_LENGTH"]		= core.getEnviromentValue("CONTENT_LENGTH");
		self.ENVIROMENT["REMOTE_ADDR"]			= core.getEnviromentValue("REMOTE_ADDR");
		self.ENVIROMENT["SERVER_NAME"]			= core.getEnviromentValue("SERVER_NAME");
		self.ENVIROMENT["SERVER_PORT"] 			= core.getEnviromentValue("SERVER_PORT");
		self.ENVIROMENT["SERVER_PROTOCOL"] 		= core.getEnviromentValue("SERVER_PROTOCOL");
		self.ENVIROMENT["SERVER_SOFTWARE"] 		= core.getEnviromentValue("SERVER_SOFTWARE");
	}
	
	parseGET = function()
	{
		self.GET = parseQueryString( core.getEnviromentValue('QUERY_STRING') );
	}
	
	parsePOST = function()
	{
		self.POST	= {};
		
		var ct		= core.getEnviromentValue("CONTENT_TYPE") || "";
		var length	= parseInt(core.getEnviromentValue("CONTENT_LENGTH"), 10);

		if (length)
		{
			if (ct.indexOf("application/x-www-form-urlencoded") != -1)
			{
				//var buffer	= this._input(length);
				self.POST	= [];
				
			}else
			{
				var boundary = ct.match(/boundary=(.*)/);
				
				if (boundary)
				{
					var body = core.ENVIROMENT['REQUEST_BODY'];
						//body = body.replace('\r\n--' + boundary[1]  + '--\r\n', '@@@@token@@@@').replace('--' + boundary[1]  + '\r\n', '@@@@token@@@@').replace(boundary[1], '@@@@token@@@@');
					var parts = body.split('--' + boundary[1]);
					
					if(parts.length > 1)
					{
						for(var i=1; i<parts.length -1; i++)
						{
							processMultipartItem(parts[i]);
						}
					}

					//parseMultipartBuffer(core.ENVIROMENT['REQUEST_BODY'], boundary[1], null);
					
					
				}
			}
		}
	}

	processMultipartItem = function(header) 
	{
		var headers		= {};
		var headerArray	= header.split("\r\n");
		
		for (var i=0, len=headerArray.length;i<len;i++)
		{
			var line = headerArray[i];
			
			if (!line) { continue; }
			
			var r = line.match(/([^:]+): *(.*)/);
			
			if (!r) { throw new Error("Malformed multipart header '"+line+"'"); }

			var name	= r[1].replace(/-/g,"_").toUpperCase();
			var value	= r[2];
			
			headers[name] = value;
		}
		
		//dump(headers);
		//dump(JSON.decode(JSON.encode(headers)));
		var hh = JSON.decode(JSON.encode(headers));
		dump(hh['CONTENT_DISPOSITION']);
		
		var cd	= headers["CONTENT_DISPOSITION"] || "";
		var ct	= headers["CONTENT_TYPE"] || "";
		var r	= cd.match(/ name="(.*?)"/i); /* form field name in header */
		
		//dump(headers["CONTENT_DISPOSITION"]);
		//dump(ct);
		//dump(r);
	}
	
	parseCookies = function() 
	{
		var result	= {};
		var all		= core.getEnviromentValue('HTTP_COOKIE').split("; ");
		var allLen	= all.length;
		
		for (var i=0; i<allLen; i++)
		{
			var row		= all[i];
			var eq		= row.indexOf("=");
			var name	= row.substring(0, eq);
			var value	= row.substring(eq + 1);
			
			if (!result[name])
			{ 
				result[name] = unescape(value); 
			}
		}
		
		self.COOKIES = result || {};
	}
	
	var self = this;

	parseENVIROMENT();
	parseGET();
	parsePOST();
	parseCookies();
};
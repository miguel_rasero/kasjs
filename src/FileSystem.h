#ifndef FILESYSTEM_H
#define FILESYSTEM_H

#include <QtCore>
#include <QFlag>

class FileSystem : public QObject
{
    Q_OBJECT

	public:
	     FileSystem(QObject *parent = 0);
	    ~FileSystem();

	protected:
		QMap<QString, QFile *>	_files;

		QFlag		mode(QString mode);
		QByteArray	encode(QString data, QString encoding);

	public slots:
		bool		dirExists(QString dirName);
		bool		dirCreate(QString dirName);
		bool		dirDelete(QString dirName);
		bool		dirDeletePath(QString dirName);
		bool		dirRename(QString dirName, QString dirNameNew);
		QStringList	dirList(QString dirName);
		QString		separator();

		bool		fileExists(QString fileName);
		QString		fileOpen(QString fileName,QString mode = "rw");
		void		fileClose(QString fileRef);
		QByteArray	fileRead(QString fileRef);
		QByteArray	fileReadLine(QString fileRef);
		void		fileWriteBinary(QString fileRef, QByteArray data);
		void		fileWrite(QString fileRef, QString data, QString encoding = "utf-8");
		void		fileCopy(QString source, QString target);
		void		fileRename(QString oldName, QString newName);
		void		fileRemove(QString fileName);
		QVariant	fileInfo(QString fileName);



		QString 	toString(QByteArray byteArray, QString encoding = "utf-8");
};

#endif

#include <QtGui>
#include <QApplication>
#include <QTimer>

#include "Engine.h"

void debugFileAppender(QtMsgType type, const char *msg)
{
	QString log;
	QString header	= "*****************************************************************************************\n";
	QString date	= QDateTime::currentDateTime().toString("yyyy.MM.dd");

	switch(type)
	{
		case QtDebugMsg:
			log = QString("%3 Debug %2\n %1").arg(msg, date, header);
			break;

		case QtWarningMsg:
			log = QString("%3Warning %2\n %1").arg(msg, date, header);
			break;

		case QtCriticalMsg:
			log = QString("%3Critical %2\n %1").arg(msg, date, header);
			break;

		case QtFatalMsg:
			log = QString("%3Fatal %2\n %1").arg(msg, date, header);
			break;//abort();
	}

	QFile fileLog("logs/kas.log");
	fileLog.open(QIODevice::WriteOnly | QIODevice::Append);

	QTextStream ts(&fileLog);
	ts << log << endl;
}

int main(int argc, char *argv[])
{
	qInstallMsgHandler(debugFileAppender);

#ifdef Q_OS_LINUX
    /*
      QtNetwork loves X, remember to use "xhost local:$cgi-server-user" in the X session running on the machine
      Another alternative would be to use xvfb-run instead
    */
    qputenv("DISPLAY", ":0.0");
#endif

	QApplication a(argc, argv);
	QTimer::singleShot(0, qApp, SLOT(quit()));
	a.exec();

	Engine engine;
	engine.serve();

	return 0;
}

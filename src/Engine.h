#ifndef ENGINE_H_
#define ENGINE_H_

#include <QtWebKit>
#include <qwaitcondition.h>

#include "Cgi.h"
#include "FileSystem.h"
#include "WebPage.h"
#include "WebkitBrowser.h"
#include "IOProcess.h"

class EngineError
{
	public:
		QString	message;

		 EngineError(QString message = "")
		 {
			 this->message = message;
         }

        ~EngineError(){}
};

class Engine : QObject
{
    Q_OBJECT
    Q_PROPERTY(QVariant HEADERS READ HEADERS)
    Q_PROPERTY(QVariant ENVIROMENT READ ENVIROMENT)
    Q_PROPERTY(QVariant GET READ GET)
    Q_PROPERTY(QVariant POST READ POST)

	public:
		 Engine(QObject *parent = 0);
		~Engine();

		void serve();

	private:
		QString					_version;
		QString					_encoding;
		QByteArray				_output;
		QString					_outputErrors;
		WebPage					_webPage;
		QMap<QString, QVariant>	_headers;
		QTimer	     			_timer;
		int         			_timeout;
		int         			_timeElapsed;

		Cgi			_cgi;

		void		resetHeaders(QString code = "200", QString type = "text/html");
		void		output(QString output);
		QByteArray	loadFile(QString fileName);
		QVariant	evaluateJS(QString javascript);

	public slots:
		QVariant HEADERS();
		QVariant ENVIROMENT();
		QVariant GET();
		QVariant POST();

		void 		setPageEncoding(QString encoding);
		void		echo(QByteArray message);
		void		echoErrors(QString message);
		void		require(QString fileName, bool silent = false);
		QString		createIntance(QString type);
		QString		getEnviromentValue(QString name);
		void		setHeader(QString header, QString value);
		void		unsetHeader(QString header);
		void		wait(int seconds);

	private slots:
		void slotUpdateTimeElapsed();
};

#endif

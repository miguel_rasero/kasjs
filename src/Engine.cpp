#include "Engine.h"

Engine::Engine(QObject *parent)
{
	this->_version		= "0.1";
	this->_encoding		= "UTF-8";
	this->_output		= "";
	this->_outputErrors	= "";
	this->_webPage.mainFrame()->addToJavaScriptWindowObject("core", this);

	connect(&this->_timer, SIGNAL(timeout()), this, SLOT(slotUpdateTimeElapsed()));
}

Engine::~Engine()
{
	this->evaluateJS(this->loadFile("klibs/downstrap.js"));
}

void Engine::serve()
{
	try
	{
		this->resetHeaders();

		this->evaluateJS(this->loadFile("klibs/bootstrap.js"));
		this->output(this->_output);

	}catch(EngineError error)
	{
		this->resetHeaders("500");
		this->output("<b>" + error.message + "</b>");

	}catch(...)
	{
		this->resetHeaders("500");
		this->output("<b>Unknown Internal Server Error</b>");
	}
}

void Engine::resetHeaders(QString code, QString type)
{
	this->_headers.clear();
	this->_headers["Status"]		= code;
	this->_headers["Content-Type"]	= type;
}

void Engine::output(QString output)
{
	QString headers = "";
	QMapIterator<QString, QVariant> i(this->_headers);
	while (i.hasNext())
	{
		i.next();
		headers += "\n" + i.key() + ": " + i.value().toString();
	}

	if(this->_outputErrors.length())
	{
		this->_outputErrors = this->_outputErrors + " ";
	}

	QByteArray response  = "Kas version: " + this->_version.toAscii();
			   response += headers;
			   response += "\n\n" + this->_outputErrors + output;



	printf(response);
}

QByteArray Engine::loadFile(QString fileName)
{
	QByteArray data;
	QFile 	file;
	file.setFileName(fileName);

	if(file.open(QIODevice::ReadOnly))
	{
		data = file.readAll();

		file.close();

	}else
	{
		EngineError error("Error. File not found (" + fileName + ")");

		throw error;
	}

	return data;
}

QVariant Engine::evaluateJS(QString javascript)
{
	return this->_webPage.mainFrame()->evaluateJavaScript(javascript);
}

QVariant Engine::HEADERS()
{
	return QVariant(this->_headers);
}

QVariant Engine::ENVIROMENT()
{
	return QVariant(this->_cgi.ENVIROMENT);
}

QVariant Engine::GET()
{
	return QVariant(this->_cgi.GET);
}

QVariant Engine::POST()
{
	return QVariant(this->_cgi.POST);
}

void Engine::setPageEncoding(QString encoding)
{
	this->_encoding = encoding;
}

void Engine::echo(QByteArray message)
{
	this->_output += message;
}

void Engine::echoErrors(QString message)
{
	this->_outputErrors += message;
}

void Engine::require(QString fileName, bool silent)
{
	QString fileContent = this->loadFile(fileName);

	if(!silent)
	{
		QString code = fileContent;

		this->evaluateJS("doJSLINT(\"" + code.replace("\r\n", "\\r").replace("\"", "'") + "\", {}, '" + fileName + "');");
	}

	this->evaluateJS(fileContent);
}

QString Engine::createIntance(QString type)
{
	QDateTime	time		= QDateTime::currentDateTime();
	QString		timestamp	= QString::number(time.toTime_t());
	QString		objName		= "obj" + timestamp;

	if(type == "fileSystem")
	{
		this->_webPage.mainFrame()->addToJavaScriptWindowObject(objName, new FileSystem(this), QScriptEngine::ScriptOwnership);

	}else if(type == "webkitBrowser")
	{
		this->_webPage.mainFrame()->addToJavaScriptWindowObject(objName, new WebkitBrowser(), QScriptEngine::QtOwnership);

	}else if(type == "IOProcess")
	{
		this->_webPage.mainFrame()->addToJavaScriptWindowObject(objName, new IOProcess(this), QScriptEngine::ScriptOwnership);

	}else
	{
		EngineError error("Error. Unknouwn object type. Type: " + type + " is not accesible.");

		throw error;
	}

	return objName;
}

QString Engine::getEnviromentValue(QString name)
{
	return QString( qgetenv(name.toStdString().c_str()) );
}

void Engine::setHeader(QString header, QString value)
{
	this->_headers[header] = value;
}

void Engine::unsetHeader(QString header)
{
	this->_headers.remove(header);
}

void Engine::wait(int seconds)
{
	this->_timeElapsed = 0;
	this->_timer.start(1000);

	while((this->_timeElapsed) < seconds)
	{
		qApp->processEvents(QEventLoop::WaitForMoreEvents | QEventLoop::ExcludeUserInputEvents);
	}

	this->_timer.stop();
}

void Engine::slotUpdateTimeElapsed()
{
	this->_timeElapsed++;
}
